package main;
import main.Camera;
import Detector.*;
import Visualizer.DesktopVisualizer;

import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.opencv.core.*;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

public class Image
{
	
	public static void showResult(Mat img) {
	    Imgproc.resize(img, img, new Size(640, 480));
	    MatOfByte matOfByte = new MatOfByte();
	    Highgui.imencode(".jpg", img, matOfByte);
	    byte[] byteArray = matOfByte.toArray();
	    BufferedImage bufImage = null;
	    try {
	        InputStream in = new ByteArrayInputStream(byteArray);
	        bufImage = ImageIO.read(in);
	        JFrame frame = new JFrame();
	        frame.getContentPane().add(new JLabel(new ImageIcon(bufImage)));
	        frame.pack();
	        frame.setVisible(true);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
	public void run() {
		
		Camera camera = new Camera();
		camera.init();
		
		DesktopVisualizer visualizer = new DesktopVisualizer();
		visualizer.init();
		
		//PeopleDetector detector = new PeopleDetector();
		//FaceDetector fDetector = new FaceDetector();
		
		ActivityDetector aDetector = new ActivityDetector();
		
		while (true) {
			
			Mat frame = camera.read();
			//frame = detector.detect(frame);
			frame = aDetector.detect(frame);
			
			visualizer.show(frame);
			
		}
		
	}
	
	public static void main( String[] args )
	{
		
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
		
		System.out.println("OpenCV Test Capture");
		
		new Image().run();
		
		
	}
}