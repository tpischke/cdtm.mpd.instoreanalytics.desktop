package main;

import org.opencv.core.Mat;
import org.opencv.highgui.VideoCapture;
import org.opencv.highgui.Highgui;

public class Camera {

	protected VideoCapture camera;
	
	public void init() {
		
		System.out.println("Initializing Camera");
		
		camera = new VideoCapture(0);
		
		camera.set(Highgui.CV_CAP_PROP_FRAME_WIDTH, 640);
	    camera.set(Highgui.CV_CAP_PROP_FRAME_HEIGHT, 480);   
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // give the camera time to initialize
		
		if(!camera.isOpened()){
	        System.out.println("Camera Error");
	    }
	    else{
	        System.out.println("Camera OK");
	    }
		
	}
	
	/**
	 * Capture a frame from the camera and return it as OpenCV Mat
	 * @return Mat 
	 */
	public Mat read() {
		
		Mat frame = new Mat();
		
		camera.read(frame);
		
		return frame;
		
	}
	
}
