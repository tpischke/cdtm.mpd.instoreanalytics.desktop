package Visualizer;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.*;

public class DesktopVisualizer {

	private JLabel label;
	//private JFrame frame;
	
	public void init() {
        
		
        label = new JLabel(new ImageIcon());
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(label);
        f.pack();
        f.setLocation(200,200);
        f.setVisible(true);
        
	}
	
	private BufferedImage frameToBufferedImage(Mat frame) {
		
		Imgproc.resize(frame, frame, new Size(640, 480));
	    MatOfByte matOfByte = new MatOfByte();
	    Highgui.imencode(".jpg", frame, matOfByte);
	    byte[] byteArray = matOfByte.toArray();
	    BufferedImage bufImage = null;
	    try {
	        InputStream in = new ByteArrayInputStream(byteArray);
	        bufImage = ImageIO.read(in);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    
	    return bufImage;
	    
	}
	
	public void show(Mat frame) {
		
        BufferedImage image = frameToBufferedImage(frame);
		
        label.setIcon(new ImageIcon(image));
        
	}
	
}
