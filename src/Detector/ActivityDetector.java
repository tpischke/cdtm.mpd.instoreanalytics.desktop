package Detector;

import org.opencv.core.*;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.imgproc.*;

public class ActivityDetector implements Detector {

	private Mat prevFrame = null;
	private Mat sumFrame = null;
	private int frames = 0;
	
	private double epsilon = 20.;
	
	public ActivityDetector() {
		
	}
	
	public Mat detect(Mat frame) {
		
		if (this.sumFrame == null) {
			this.sumFrame = new Mat(frame.rows(), frame.cols(), CvType.CV_32SC1);
		}
		
		Mat diff = new Mat(frame.rows(), frame.cols(), frame.type());
		Mat frameGray = new Mat(frame.rows(), frame.cols(), frame.type());
		
		//Imgproc.cvtColor(frame, diff, Imgproc.COLOR_RGB2GRAY);
		Imgproc.cvtColor(frame, frameGray, Imgproc.COLOR_RGB2GRAY);
		
		if (this.prevFrame != null) {
			
			//System.out.println(diff.size());
			//System.out.println(this.prevFrame.size());
			
			// calculate distance to each point of the matrix from the previous frame
			Core.absdiff(frameGray, this.prevFrame, diff);
			
			// compare if the values are above 
			Core.compare(diff, new Scalar(epsilon), diff, Core.CMP_GT);
			
			Mat frameConverted = new Mat(frame.rows(), frame.cols(), CvType.CV_32SC1);
			diff.convertTo(frameConverted, CvType.CV_32SC1);
			
			// add them to the sum
			Core.add(frameConverted, sumFrame, sumFrame);
			
		}
		
		this.prevFrame = frameGray.clone();
		
		//Mat frameConverted = new Mat(frame.rows(), frame.cols(), CvType.CV_32SC1);
		//frameGray.convertTo(frameConverted, CvType.CV_32SC1);
		
		frames++;
		System.out.println(frames);
		if (frames > 200) {
			return this.applyJetColor(sumFrame);
		}
		
		return diff;
	}
	
	public Mat applyJetColor(Mat mat) {
		
		Mat heatmap = new Mat(mat.rows(), mat.cols(), CvType.CV_8UC3);
		
		MinMaxLocResult res = Core.minMaxLoc(mat);
		double max = res.maxVal;
		//System.out.println(max);
		//return frame;
		
		for (int i = 0; i < mat.rows(); i++) {
			for (int j = 0; j < mat.cols(); j++) {
				double percent = mat.get(i, j)[0] / max;
				double[] color = mapJetColor(percent);
				//System.out.println(color);
				heatmap.put(i, j, color);
			}
		}
		
		return heatmap;
		
	}

	protected double[] mapJetColor(double percent) {
		
		double r, g, b;
		
		r = 255.*percent;
		g = 0.;
		b = 0.;
		
		double [] color = new double[3];
		color[0] = b;
		color[1] = g;
		color[2] = r;
		
 		return color;
	}
	
}
