package Detector;

import org.opencv.core.*;
import org.opencv.objdetect.HOGDescriptor;

public class PeopleDetector implements Detector {
	
	public Mat detect(Mat frame) {
		
		HOGDescriptor hog = new HOGDescriptor();
		hog.setSVMDetector(HOGDescriptor.getDefaultPeopleDetector());
		
		MatOfRect peopleDetections = new MatOfRect();
		MatOfDouble peopleDetectionWeights = new MatOfDouble();
		
		hog.detectMultiScale(frame, peopleDetections, peopleDetectionWeights);
		
		System.out.println(String.format("Detected %s people", peopleDetections.toArray().length));
		
		// Draw a bounding box around each face.
	    for (Rect rect : peopleDetections.toArray()) {
	        Core.rectangle(frame, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
	    }
		
		return frame;
		
	}
	
}
