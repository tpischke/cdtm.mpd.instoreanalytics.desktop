package Detector;

import org.opencv.core.Mat;

public interface Detector {

	public Mat detect(Mat frame);
	
}
