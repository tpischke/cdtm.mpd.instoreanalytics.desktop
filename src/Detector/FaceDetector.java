package Detector;

import org.opencv.core.*;
import org.opencv.objdetect.CascadeClassifier;

public class FaceDetector implements Detector {
	
	public Mat detect(Mat frame) {
		
		CascadeClassifier faceDetector = new CascadeClassifier(getClass().getResource("lbpcascade_frontalface.xml").getPath());
		MatOfRect faceDetections = new MatOfRect();
		faceDetector.detectMultiScale(frame, faceDetections);
		
		System.out.println(String.format("Detected %s faces", faceDetections.toArray().length));
		
		// Draw a bounding box around each face.
	    for (Rect rect : faceDetections.toArray()) {
	        Core.rectangle(frame, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
	    }
		
		return frame;
		
	}
	
}
